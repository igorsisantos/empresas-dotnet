﻿using System;
using System.Collections.Generic;
using System.Text;
using Filme.Business.Interfaces;
using Filme.Business.Models;
using Filme.Data.Context;

namespace Filme.Data.Repository
{
    public class DiretorRepository : Repository<Diretor>, IDiretorRepository
    {
        public DiretorRepository(MeuDbContext context) : base(context) { }
    }
}
