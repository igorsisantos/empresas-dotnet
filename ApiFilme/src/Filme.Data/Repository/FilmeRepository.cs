﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Filme.Business.Interfaces;
using Filme.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace Filme.Data.Repository
{
    public class FilmeRepository : Repository<Business.Models.Filme>, IFilmeRepository
    {
        public FilmeRepository(MeuDbContext context) : base(context)
        {

        }
    }
}
