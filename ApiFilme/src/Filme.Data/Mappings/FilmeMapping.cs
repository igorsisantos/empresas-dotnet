﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Filme.Data.Mappings
{
    public class FilmeMapping : IEntityTypeConfiguration<Business.Models.Filme>
    {
        public void Configure(EntityTypeBuilder<Business.Models.Filme> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Nome)
                .IsRequired()
                .HasColumnType("varchar(200)");

            builder.Property(p => p.Genero)
                .IsRequired()
                .HasColumnType("varchar(200)");

            builder.Property(p => p.QtdVoto)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(p => p.MediaVoto)
                .IsRequired()
                .HasColumnType("decimal");

            builder.ToTable("Filme");
        }
    }
}
