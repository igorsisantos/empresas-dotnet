﻿using Filme.Business.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace Filme.Data.Mappings
{
    public class VotoMapping : IEntityTypeConfiguration<Voto>
    {
        public void Configure(EntityTypeBuilder<Voto> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Nota)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(p => p.UserId)
                .IsRequired()
                .HasColumnType("varchar(200)");

            builder.ToTable("Voto");
        }
    }
}
