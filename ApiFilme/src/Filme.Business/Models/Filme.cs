﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Filme.Business.Models
{
    public class Filme : Entity
    {
        public string Nome { get; set; }

        public string Genero { get; set; }

        public int QtdVoto { get; set; }

        public Decimal MediaVoto { get; set; }

        /* EF Relations */
        public IEnumerable<Diretor> Diretores { get; set; }

        public IEnumerable<Ator> Atores { get; set; }

        public IEnumerable<Voto> Votos { get; set; }
    }
}