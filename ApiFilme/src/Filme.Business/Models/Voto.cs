﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Filme.Business.Models
{
    public class Voto : Entity
    {
        public Guid FilmeId { get; set; }
        public Guid UserId { get; set; }
        public int Nota { get; set; }

        /* EF Relations */
        public Filme Filme { get; set; }
    }
}
