﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Filme.Business.Models
{
    public class Ator : Entity
    {
        public Guid FilmeId { get; set; }
        public string Nome { get; set; }

        /* EF Relations */
        public Filme Filme { get; set; }
    }
}
