﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Filme.Business.Interfaces;
using Filme.Business.Models;

namespace Filme.Business.Services
{
    public class VotoService : BaseService, IVotoService
    {
        private readonly IVotoRepository _votoRepository;
      

        public VotoService(IVotoRepository votoRepository,
            INotificador notificador) : base(notificador)
        {
            _votoRepository = votoRepository;
        }

        public async Task<bool> Adicionar(Voto voto)
        {
            await _votoRepository.Adicionar(voto);

            return true;
        }

        public void Dispose()
        {
            _votoRepository?.Dispose();
        }
    }
}
