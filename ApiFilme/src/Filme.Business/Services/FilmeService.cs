﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Filme.Business.Interfaces;

namespace Filme.Business.Services
{
    public class FilmeService : BaseService, IFilmeService
    {
        private readonly IFilmeRepository _filmeRepository;

        public FilmeService(IFilmeRepository filmeRepository,
            INotificador notificador) : base(notificador)
        {
            _filmeRepository = filmeRepository;
        }

        public async Task<bool> Adicionar(Models.Filme filme)
        {
            await _filmeRepository.Adicionar(filme);

            return true;
        }


        public async Task<Models.Filme> BuscarId(Guid id)
        {
            return await _filmeRepository.ObterPorId(id);
        }

        public async Task<List<Models.Filme>> BuscarTodos()
        {
            return await _filmeRepository.ObterTodos();
        }

        public async Task AtualizarVotos(Models.Filme filme)
        {
            await _filmeRepository.Atualizar(filme);
        }

        public void Dispose()
        {
            _filmeRepository?.Dispose();
        }
    }
}
