﻿using System;
using System.Collections.Generic;
using System.Text;
using Filme.Business.Notificacoes;

namespace Filme.Business.Interfaces
{
    public interface INotificador
    {
        bool TemNotificacao();
        List<Notificacao> ObterNotificacoes();
        void Handle(Notificacao notificacao);
    }
}
