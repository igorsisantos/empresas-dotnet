﻿using System;
using System.Collections.Generic;
using System.Text;
using Filme.Business.Models;

namespace Filme.Business.Interfaces
{
    public interface IFilmeRepository : IRepository<Models.Filme>
    {

    }
}
