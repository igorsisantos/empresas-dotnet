﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Filme.Business.Models;

namespace Filme.Business.Interfaces
{
    public interface IVotoService : IDisposable
    {
        Task<bool> Adicionar(Voto filme);
    }
}
