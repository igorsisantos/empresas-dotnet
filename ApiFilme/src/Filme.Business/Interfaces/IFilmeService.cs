﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Filme.Business.Models;

namespace Filme.Business.Interfaces
{
    public interface IFilmeService : IDisposable
    {
        Task<bool> Adicionar(Models.Filme filme);

        Task<List<Models.Filme>> BuscarTodos();

        Task<Models.Filme> BuscarId(Guid id);

        Task AtualizarVotos(Models.Filme filme);
    }
}
