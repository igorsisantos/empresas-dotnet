﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Filme.Api.ViewModels;
using Filme.Business.Models;

namespace Filme.Api.Configuration
{
    public class AutomapperConfig : Profile
    {
        public AutomapperConfig()
        {
            CreateMap<Business.Models.Filme, FilmeViewModel>().ReverseMap();

            CreateMap<Ator, AtorViewModel>().ReverseMap();

            CreateMap<Diretor, DiretorViewModel>().ReverseMap();

            CreateMap<Voto, VotoViewModel>().ReverseMap();
        }
    }
}
