﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Filme.Api.Extensions;
using Filme.Business.Interfaces;
using Filme.Business.Notificacoes;
using Filme.Business.Services;
using Filme.Data.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Filme.Data.Repository;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Filme.Api.Configuration
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection ResolveDependencies(this IServiceCollection services)
        {
            services.AddScoped<MeuDbContext>();
            services.AddScoped<IFilmeRepository, FilmeRepository>();
            services.AddScoped<IVotoRepository, VotoRepository>();

            services.AddScoped<INotificador, Notificador>();
            services.AddScoped<IFilmeService, FilmeService>();
            services.AddScoped<IVotoService, VotoService>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUser, AspNetUser>();

            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();

            return services;
        }
    }
}
