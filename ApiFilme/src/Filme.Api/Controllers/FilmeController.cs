﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Filme.Api.Extensions;
using Filme.Api.ViewModels;
using Filme.Business.Interfaces;
using Filme.Business.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Filme.Api.Controllers
{
    [Authorize]
    [Route("api/filme")]
    public class FilmeController : MainController
    {
        private readonly IFilmeService _filmeService;
        private readonly IVotoService _votoService;
        private readonly IFilmeRepository _filmeRepository;
        private readonly IMapper _mapper;

        public FilmeController(IFilmeService filmeService,
                                IVotoService votoService,
                                IFilmeRepository filmeRepository,
                                IMapper mapper,
                                INotificador notificador,
                                IUser user) : base(notificador, user)
        {
            _filmeService = filmeService;
            _mapper = mapper;
            _votoService = votoService;
            _filmeRepository = filmeRepository;
        }

        [ClaimsAuthorize("administrador", "cadastrar")]
        [HttpPost("cadastrar")]
        public async Task<ActionResult<FilmeViewModel>> AdicionarFilme(FilmeViewModel filmeViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            await _filmeService.Adicionar(_mapper.Map<Business.Models.Filme>(filmeViewModel));

            return CustomResponse(filmeViewModel);
        }

        [ClaimsAuthorize("usuario", "voto")]
        [HttpPost("voto")]
        public async Task<ActionResult<FilmeViewModel>> AdicionarVoto(VotoViewModel votoViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var response = await _votoService.Adicionar(_mapper.Map<Voto>(votoViewModel));

            if (response)
            {
                var filme = await _filmeService.BuscarId(votoViewModel.FilmeId);

                filme.QtdVoto += 1;

                filme.MediaVoto = Decimal.Divide(Convert.ToDecimal(filme.MediaVoto + votoViewModel.Nota),
                    Convert.ToDecimal(filme.QtdVoto));

                await _filmeService.AtualizarVotos(filme);
            }

            return CustomResponse(votoViewModel);
        }


        [HttpGet("buscar")]
        public async Task<IEnumerable<FilmeViewModel>> BuscarFilter(string nome, string genero)
        {
            return _mapper.Map<List<FilmeViewModel>>(await _filmeRepository.Buscar(f => f.Genero.Equals(genero) || f.Nome.Equals(nome)));
        }

        [HttpGet("buscar-paginado")]
        public async Task<PagedResult<Business.Models.Filme>> BuscarPaginado([FromQuery] int ps= 8,[FromQuery] int page = 1, [FromQuery] string q = null)
        {
            return (await _filmeRepository.ObterTodosPaginado(ps, page, q));
        }

        [HttpGet("mais-votados")]
        public async Task<ActionResult> BuscarMaisVotados()
        {
            var list = _mapper.Map<List<FilmeViewModel>>(await _filmeService.BuscarTodos());

            var filmeViewModels = list.OrderByDescending(c => c.QtdVoto).ThenBy(n => n.Nome).ToList();

            return CustomResponse(filmeViewModels);
        }

        [HttpGet("detalhes")]
        public async Task<FilmeViewModel> Detalhes(Guid id)
        {
            return _mapper.Map<FilmeViewModel>(await _filmeService.BuscarId(id));
        }


    }
}
