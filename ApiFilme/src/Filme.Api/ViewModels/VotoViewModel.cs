﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Filme.Api.ViewModels
{
    public class VotoViewModel
    {
        [Key]
        public Guid Id { get; set; }

        public int Nota { get; set; }

        public Guid FilmeId { get; set; }

        public Guid UserId { get; set; }
    }
}
