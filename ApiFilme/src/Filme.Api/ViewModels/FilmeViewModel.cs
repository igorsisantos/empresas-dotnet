﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Filme.Api.ViewModels
{
    public class FilmeViewModel
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [StringLength(100, ErrorMessage = "O campo {0} precisa ter entre {2} e {1} caracteres", MinimumLength = 2)]
        public string Nome { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [StringLength(100, ErrorMessage = "O campo {0} precisa ter entre {2} e {1} caracteres", MinimumLength = 2)]
        public string Genero { get; set; }

        public decimal MediaVoto { get; set; }

        public int QtdVoto { get; set; }

        public IEnumerable<AtorViewModel> Atores { get; set; }

        public IEnumerable<DiretorViewModel> Diretores { get; set; }

        public IEnumerable<VotoViewModel> Votos { get; set; }
    }
}
